-- This header is used to feed the inner scope of the addon with links to the global environment.
-- If you need additional API from wow you need to start by adding it here and then you can update the types to reflect this change.
__TS__FunctionCall = function(fn, thisArg, ...)
	local args = ({ ... });
	return fn(thisArg, (unpack or table.unpack)(args));
end;

function AddTolib(lib, name, item) 
    if not lib then return end
    lib[name] = item
end
local MinionPlayer = Player
local MinionTarget = Target