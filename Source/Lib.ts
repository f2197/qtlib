import { Core } from "./Core";
// Content //
const QtLib = Core;
_G["QtLib"] = QtLib;
RegisterEventHandler("Module.Initalize", () => QtLib.Init(), "QtLib.Init")
RegisterEventHandler("Gameloop.Update", () => QtLib.Update(), "QtLib.Update")
RegisterEventHandler("Gameloop.Draw", () => QtLib.MainWindow(), "QtLib.MainWindow")
RegisterEventHandler("Game.UIEvent", (name: string, payload: string) => d("UI Event: " + name, "Payload: " + payload), "QtLib.UIEvent")

