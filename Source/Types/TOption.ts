// Content //

export type TOption = "Checkbox" | "InputText" | "InputTextEditor" | "TreeNode" | "InputInt" | "InputFloat" | "InputTextMultiLine";