## Set the addon name to parameter the output addon's identity
Name=qtlib

## Version number
Version=1

## Dependencies seperated by comma
Dependencies=minionlib


## This file will be used to add new files to be wrapped. 
## The Wrapper will always find every files that are listed and add these one by one, like a normal .toc would do.

## Files

Types/TOption.ts

Interfaces/IMinionAddonData.ts
Interfaces/IAddonInfo.ts
Interfaces/UserInterface/IGUI.ts
Interfaces/UserInterface/ISetting.ts
Interfaces/UserInterface/IWindowStyle.ts
Interfaces/UserInterface/IOptionPayload.ts
Interfaces/UserInterface/ICheckboxPayload.ts
Interfaces/UserInterface/ITooltip.ts
Interfaces/IACRGUI.ts
Interfaces/IACRPayload.ts

API/API.ts
API/Entities.TS
API/UI.ts
API/Math.ts

Models/UserInterface/Page.ts
Models/UserInterface/MainWindow.ts
Models/UserInterface/TabbedWindow.ts

Models/UserInterface/Options/Abstracts/Option.ts
Models/UserInterface/Options/Checkbox.ts
Models/UserInterface/Options/InputText.ts
Models/UserInterface/Options/InputTextMultiLine.ts
Models/UserInterface/Options/InputTextEditor.ts
Models/UserInterface/Options/InputInt.ts
Models/Unit.ts
Models/PlayerModel.ts
Models/TargetModel.ts

Models/Actions/Action.ts

Init.ts
Core.ts
Lib.ts
Jobs/Bard.ts
Jobs/BlackMage.ts
Jobs/Gunbreaker.ts



