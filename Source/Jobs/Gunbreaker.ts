import { minionGUI } from "../Init";
import { Checkbox } from "../Models/UserInterface/Options/Checkbox";
import { Core } from "../Core";
import { Page } from "../Models/UserInterface/Page";
import { TabbedWindow } from "../Models/UserInterface/TabbedWindow";
import { API } from "../API/API";
import { Option } from "../Models/UserInterface/Options/Abstracts/Option";
import { Action } from "../Models/Actions/Action";
import { InputInt } from "../Models/UserInterface/Options/InputInt";
import { Entities } from "../API/Entities"

// Content //
export namespace Gunbreaker {
    const Player = Core.Player;
    export const GUI = {
        open: false,
        visible: false,
        name: "QtGNB"
    }
    export namespace Actions {
        // Offensives 
        export const KeenEdge = new Action({
            identifier: 16137,
            hostile: true, instant: true
        })
        export const BrutalShell = new Action({
            identifier: 16139,
            hostile: true, instant: true,
            prevComboAction: KeenEdge
        })
        export const SolidBarrel = new Action({
            identifier: 16145,
            hostile: true, instant: true,
            prevComboAction: BrutalShell
        });
        export const LightningShot = new Action({
            identifier: 16143,
            hostile: true, instant: true
        })
        export const BurstStrike = new Action({
            identifier: 16162,
            hostile: true, instant: true
        })
        export const GnashingFang = new Action({
            identifier: 16146,
            hostile: true, instant: true
        })
        export const SavageClaw = new Action({
            identifier: 16147,
            hostile: true, instant: true,
            prevComboAction: GnashingFang
        })
        export const WickedTalon = new Action({
            identifier: 16150,
            hostile: true, instant: true,
            prevComboAction: SavageClaw
        })
        export const DemonSlice = new Action({
            identifier: 16141,
            offGcd: true
        })
        export const DemonSlaughter = new Action({
            identifier: 16149,
            instant: true,
            prevComboAction: DemonSlice
        })
        export const BowShock = new Action({
            identifier: 16159,
            instant: true
        })
        export const FatedCircle = new Action({
            identifier: 16163,
            instant: true
        })
        export const DoubleDown = new Action({
            identifier: 25760,
            instant: true
        })

        // Offensive OffGCD
        export const DangerZone = new Action({
            identifier: 16144,
            hostile: true, instant: true, offGcd: true
        })
        export const SonicBreak = new Action({
            identifier: 16153,
            hostile: true, instant: true, offGcd: true
        })
        export const RoughDivide = new Action({
            identifier: 16154,
            hostile: true, instant: true, offGcd: true
        })
        export const BlastingZone = new Action({
            identifier: 16165,
            hostile: true, instant: true, offGcd: true
        })
        export const JugularRip = new Action({
            identifier: 16156,
            hostile: true, instant: true, offGcd: true,
            auraId: 1842
        })
        export const AbdomenTear = new Action({
            identifier: 16157,
            hostile: true, instant: true, offGcd: true,
            auraId: 1843
        })
        export const EyeGouge = new Action({
            identifier: 16158,
            hostile: true, instant: true, offGcd: true,
            auraId: 1844
        })
        export const Hypervelocity = new Action({
            identifier: 25759,
            hostile: true, instant: true, offGcd: true,
            auraId: 2686
        })

        // Defensives
        export const Camouflage = new Action({
            identifier: 16140,
            instant: true, offGcd: true
        })
        export const Nebula = new Action({
            identifier: 16148,
            instant: true, offGcd: true
        })
        export const Aurora = new Action({
            identifier: 16151,
            instant: true, offGcd: true
        })
        export const Superbolide = new Action({
            identifier: 16152,
            instant: true, offGcd: true
        })
        export const HeartOfLight = new Action({
            identifier: 16160,
            instant: true, offGcd: true
        })
        export const HeartOfStone = new Action({
            identifier: 16161,
            instant: true, offGcd: true
        })
        export const HeartOfCorundum = new Action({
            identifier: 25758,
            instant: true, offGcd: true
        })
        // Cooldowns
        export const NoMercy = new Action({
            identifier: 16138,
            instant: true, offGcd: true,
            auraId: 1831
        })
        export const Bloodfest = new Action({
            identifier: 16164,
            instant: true, offGcd: true
        })

        // Buffs
        export const RoyalGuard = new Action({
            identifier: 16142,
            auraId: 1833, instant: true, offGcd: true
        })


    }
    export namespace Methods {
        export function NumCartridges(): number {
            return Player.GetGauge(1) || 0;
        }
    }
    export namespace Variables {
        export let PoolingForDoubleDown: boolean = false;
    }
    export namespace UserInterface {
        export namespace Windows {
            export const main = new TabbedWindow({
                size: { width: 500, height: 400 },
                tabs: GUI_CreateTabs("General, Offensive, Defensive, Debug")
            })
        }

        export namespace Options {
            export function Draw(options: { [key: string]: Option }): void {
                API.ObjectForEach(options, (k, v) => {
                    v.Draw();
                });
            }
            export function Save(options: { [key: string]: Option }): void {
                API.ObjectForEach(options, (k, v) => {
                    v.Save();
                });
            }
            export namespace General {
                export const cum = new Checkbox("Cum", false);
            }
            export namespace Offensive {
                export const aoe = new Checkbox("AOE", true);
                export const startAttack = new Checkbox("StartAttack", false);
                export const cds = new Checkbox("CDs", true);
                export const doubleDown = new Checkbox("Double Down", true);
                export const noMercy = new Checkbox("No Mercy", true);
                export const bloodfest = new Checkbox("Bloodfest", true);
                export const fatedCircleUnits = new InputInt("Fated Circle", 3)
                export const aoeUnits = new InputInt("Aoe Combo", 2)
            }
            export namespace Defensive {
                export const balls = new Checkbox("Balls", false);
            }
        }
    }

    export const classes = {
        [FFXIV.JOBS.GUNBREAKER]: true,
    }
    export function Cast(): boolean {
        const Target = Player.Target;
        if (Target && Player.DistanceTo(Target) <= 5) {
            if (UserInterface.Options.Offensive.noMercy.Checked && Methods.NumCartridges() > 1 && Actions.Bloodfest.Cooldown < 6 && Actions.NoMercy.CanCast(Target.Identifier)) {
                Core.Log("No Mercy");
                Actions.NoMercy.Cast(Target.Identifier);
                return true;
            }
            if (Player.Aura(Actions.NoMercy.AuraId)) {
                if (UserInterface.Options.Offensive.bloodfest.Checked && Methods.NumCartridges() < 1 && Actions.Bloodfest.CanCast(Target.Identifier)) {
                    Core.Log("Bloodfest");
                    Actions.Bloodfest.Cast(Target.Identifier);
                    return true;
                }
                if (Actions.GnashingFang.IsOnCooldown) {
                    if (UserInterface.Options.Offensive.doubleDown.Checked && Methods.NumCartridges() > 1 && Actions.DoubleDown.CanCast(Target.Identifier)) {
                        Core.Log("Double Down");
                        Actions.DoubleDown.Cast(Target.Identifier);
                        return true;
                    }
                    if (Actions.DangerZone.CanCast(Target.Identifier)) {
                        Core.Log("Danger Zone");
                        Actions.DangerZone.Cast(Target.Identifier);
                        return true;
                    }
                    if (Actions.SonicBreak.CanCast(Target.Identifier)) {
                        Core.Log("Sonic Break");
                        Actions.SonicBreak.Cast(Target.Identifier);
                        return true;
                    }
                    if (Actions.BowShock.CanCast(Target.Identifier)) {
                        Core.Log("Bow Shock");
                        Actions.BowShock.Cast(Target.Identifier);
                        return true;
                    }

                }
            }
            if (Player.Aura(Actions.EyeGouge.AuraId) && Actions.EyeGouge.CanCast(Target.Identifier)) {
                Core.Log("Eye Gouge");
                Actions.EyeGouge.Cast(Target.Identifier);
                return true;
            }
            if (Player.Aura(Actions.AbdomenTear.AuraId) && Actions.AbdomenTear.CanCast(Target.Identifier)) {
                Core.Log("Abdomen Tear");
                Actions.AbdomenTear.Cast(Target.Identifier);
                return true;
            }
            if (Player.Aura(Actions.JugularRip.AuraId) && Actions.JugularRip.CanCast(Target.Identifier)) {
                Core.Log("Jugular Rip");
                Actions.JugularRip.Cast(Target.Identifier);
                return true;
            }
            if (Player.Aura(Actions.Hypervelocity.AuraId) && Actions.Hypervelocity.CanCast(Target.Identifier)) {
                Core.Log("Hypervelocity");
                Actions.Hypervelocity.Cast(Target.Identifier);
                return true;
            }
            if (Actions.WickedTalon.CanCombo() && Actions.SolidBarrel.CanCast(Target.Identifier)) {
                Core.Log("Wicked Talon");
                Actions.WickedTalon.Cast(Target.Identifier);
                return true;
            }
            if (Actions.SavageClaw.CanCombo() && Actions.SavageClaw.CanCast(Target.Identifier)) {
                Core.Log("Savage Glaw");
                Actions.SavageClaw.Cast(Target.Identifier);
                return true;
            }
            Variables.PoolingForDoubleDown = Actions.DoubleDown.IsUsable && Actions.DoubleDown.Cooldown < 5 && Methods.NumCartridges() < 3;
            if (Methods.NumCartridges() > 0 && !Variables.PoolingForDoubleDown) {
                if (Actions.GnashingFang.CanCast(Target.Identifier)) {
                    Core.Log("Gnashing Fang");
                    Actions.GnashingFang.Cast(Target.Identifier);
                    return true;
                }
                if (Actions.BurstStrike.CanCast(Target.Identifier)) {
                    Core.Log("Burst Strike");
                    Actions.BurstStrike.Cast(Target.Identifier);
                    return true;
                }
            }
            if (Actions.FatedCircle.UnitsCount >= UserInterface.Options.Offensive.fatedCircleUnits.Value && Methods.NumCartridges() > 0) {
                if (Actions.FatedCircle.CanCast(Target.Identifier)) {
                    Actions.FatedCircle.Cast(Target.Identifier);
                    return true;
                }
            }
            if (Actions.DemonSlice.UnitsCount >= UserInterface.Options.Offensive.aoeUnits.Value) {
                if (Actions.DemonSlaughter.CanCombo() && Actions.DemonSlaughter.CanCast(Target.Identifier)) {
                    Actions.DemonSlaughter.Cast(Target.Identifier);
                    return true;
                }
                if (Actions.DemonSlice.CanCast(Target.Identifier)) {
                    Actions.DemonSlice.Cast(Target.Identifier);
                    return true;
                }
            }
            if (Actions.SolidBarrel.CanCombo() && Actions.SolidBarrel.CanCast(Target.Identifier)) {
                Core.Log("Solid Barrel");
                Actions.SolidBarrel.Cast(Target.Identifier);
                return true;
            }
            if (Actions.BrutalShell.CanCombo() && Actions.BrutalShell.CanCast(Target.Identifier)) {
                Core.Log("Brutal Shell");
                Actions.BrutalShell.Cast(Target.Identifier);
                return true;
            }
            if (Actions.KeenEdge.CanCast(Target.Identifier)) {
                Core.Log("Keen Edge");
                Actions.KeenEdge.Cast(Target.Identifier);
                return true;
            }
        }
        return false;
    }
    export function Draw(): void {
        if (GUI.open) {
            const [vis, open] = minionGUI.Begin(GUI.name, GUI.open);
            GUI.visible = vis;
            GUI.open = open;
            if (GUI.visible) {
                const [tabIndex, tabName] = UserInterface.Windows.main.DrawTabs();
                // General
                if (tabIndex == 1) {
                    UserInterface.Options.Draw(UserInterface.Options.General);
                    UserInterface.Options.Save(UserInterface.Options.General);
                }
                // Offesnive
                if (tabIndex == 2) {
                    UserInterface.Options.Draw(UserInterface.Options.Offensive);
                    UserInterface.Options.Save(UserInterface.Options.Offensive);
                }
                // Defensive
                if (tabIndex == 3) {
                    UserInterface.Options.Draw(UserInterface.Options.Defensive);
                    UserInterface.Options.Save(UserInterface.Options.Defensive);
                }
                if (tabIndex == 4) {
                    Core.LogWindow([
                        {
                            id: "Num Cartridges",
                            value: Methods.NumCartridges()
                        },
                        {
                            id: "LastCast",
                            value: Player.CastingInfo?.lastcastid || -1
                        },
                        {
                            id: "LastCombo",
                            value: Player.base.lastcomboid || -1
                        },

                        {
                            id: "Has No Mercy",
                            value: Player.Aura(Actions.NoMercy.AuraId)
                        },
                        {
                            id: "Has Royal Guard",
                            value: Player.Aura(Actions.RoyalGuard.AuraId)
                        },
                        {
                            id: "Has R2R",
                            value: Player.Aura(Actions.JugularRip.AuraId)
                        },
                        {
                            id: "Has AT",
                            value: Player.Aura(Actions.AbdomenTear.AuraId)
                        },
                        {
                            id: "Has Gouge",
                            value: Player.Aura(Actions.EyeGouge.AuraId)
                        },
                        {
                            id: "Enemies 8",
                            value: Player.NumUnitsAround(8)
                        },
                        {
                            id: "Enemies 10",
                            value: Player.NumUnitsAround(10)
                        },
                        {
                            id: "Enemies 12",
                            value: Player.NumUnitsAround(12)
                        },
                        {
                            id: "Combo Time Remaining",
                            value: Player.ComboTimeRemaining
                        }
                    ]);
                    let checkSpells: [string[], Action[]] = [
                        [
                            "KeenEdge",
                            "BrutalShell",
                            "SolidBarrel",
                            "GnashingFang",
                            "SavageClaw",
                            "WickedTalon",
                        ],
                        [
                            Actions.KeenEdge, Actions.BrutalShell, Actions.SolidBarrel,
                            Actions.GnashingFang, Actions.SavageClaw, Actions.WickedTalon
                        ]]
                    for (let i = 0; i < checkSpells[0].length; i++) {
                        const thisName = checkSpells[0][i];
                        const thisSpell = checkSpells[1][i];
                        Core.LogWindow([
                            {
                                id: thisName + " Ready",
                                value: thisSpell.action.IsReady()
                            },
                            {
                                id: thisName + " comboAction",
                                value: thisSpell.comboAction && thisSpell.comboAction.TimeSinceCast || -1
                            },
                            {
                                id: thisName + " cd",
                                value: thisSpell.Cooldown
                            },
                            {
                                id: thisName + " Combo Spell ID",
                                value: thisSpell.action.combospellid
                            },
                            {
                                id: thisName + " CanCast Result",
                                value: thisSpell.action.CanCastResult()
                            },
                            {
                                id: thisName + " base usable",
                                value: thisSpell.action.usable
                            },

                        ])
                    }
                    if (Player.Target) {
                        Core.LogWindow([
                            {
                                id: "Target Enemies 5",
                                value: Player.Target.UnitsAround(5, "alive,Targetable,los,maxdistance=40").length
                            },
                            {
                                id: "DeathTracker Size",
                                value: Object.keys(Entities.UnitHealthTracker).length
                            },
                            {
                                id: "Target TTD",
                                value: Player.Target.UnitTimeToX(0)
                            },
                            {
                                id: "Distance To Target",
                                value: Player.DistanceTo(Player.Target)
                            },
                        ]);
                    }

                }

            }
            minionGUI.End();
        }
    }
    export function DrawHeader(): void { };
    export function DrawFooter(): void { };
    export function OnOpen(): void {
        GUI.open = true;
    }
    export function OnLoad(): void {

    }
    export function OnClick(mouse: number, shiftState: boolean, controlState: boolean, altState: boolean, entity: object): void { }
    export function OnUpdate(event: any, tickcount: number) { }
    function Init(): void {
    }
    Core.ACRs.push(Gunbreaker)
}