import { minionGUI } from "../Lib";
import { IACRGUI } from "../Interfaces/IACRGUI";
import { IACRPayload } from "../Interfaces/IACRPayload";
import { Checkbox } from "../Models/UserInterface/Options/Checkbox";
import { Core } from "../Core";

// Content //
export namespace Bard {
    export namespace Options {
        export const Aoe = new Checkbox("Aoe", false);
        export const StartAttack = new Checkbox("StartAttack", false);
        export const CDs = new Checkbox("CDs", false);
    }
    export const GUI = {
        open: false,
        visible: false,
        name: "QtBard"
    }
    export const classes = {
        [FFXIV.JOBS.ARCHER]: true,
        [FFXIV.JOBS.BARD]: true,
    }

    export function Cast(): boolean {
        return false;
    }
    export function Draw(): void {
        if (GUI.open) {
            const [vis, open] = minionGUI.Begin(GUI.name, GUI.open);
            GUI.visible = vis;
            GUI.open = open;
            if (GUI.visible) {
            }
            minionGUI.End();
        }
    }
    export function DrawHeader(): void { };
    export function DrawFooter(): void { };
    export function OnOpen(): void {
        GUI.open = true;
    }
    export function OnLoad(): void {

    }
    export function OnClick(mouse: number, shiftState: boolean, controlState: boolean, altState: boolean, entity: object): void { }
    export function OnUpdate(event: any, tickcount: number) { }
    function Init(): void {
    }
    Core.ACRs.push(Bard)
}