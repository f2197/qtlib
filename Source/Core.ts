import { API } from "./API/API";
import { minionGUI } from "./Init";
import { PlayerModel } from "./Models/PlayerModel";
import { TargetModel } from "./Models/TargetModel";
import { Checkbox } from "./Models/UserInterface/Options/Checkbox";
import { InputText } from "./Models/UserInterface/Options/InputText";
import { InputTextEditor } from "./Models/UserInterface/Options/InputTextEditor";
import { InputTextMultiLine } from "./Models/UserInterface/Options/InputTextMultiLine";



// Content //
export namespace Core {
    export const Player = new PlayerModel(MinionPlayer);
    export const Target = new TargetModel(MGetTarget() || MinionTarget);
    export const DistancesTable: { [Guid: string]: { [Guid: string]: [number, number] } } = {};
    export const luaPath = GetLuaModsPath();
    export const info = {
        author: "The QT",
        addonName: "QtLib",
        className: "QtLib",
        version: 1,
        startDate: "01-01-2021",
        lastUpdate: "01-01-2021",
        description: "QTLib Main Button",
        changelog: [
            { version: "0.0.1", description: "Starting development." }
        ]
    };
    export let minionSettings = luaPath + "ffxivminion\\";
    export const modulePath = luaPath + info.className + "\\";
    export const moduleSettingPath = luaPath + "ffxivminion\\" + info.className + "\\";
    export const settingsPath = luaPath + "ffxivminion\\" + info.className + "\\" + "QtLib.lua";
    export const defaultSettings = {
        version: info.version
    };
    export let settings = {};
    export let saveLastCheck = Now();
    export const GUI = {
        Open: false,
        Visible: true,
        ToolTip: info.description,
        IsOpen: IsMainWindowOpen,
        OnClick: ToggleMainWindow,
    };
    export const style = {
        mainWindow: {
            size: { width: 500, height: 400 },
            components: { mainTabs: GUI_CreateTabs("General") },
        }
    }


    export namespace Helpers {
        /** Makes a column group in the current context
         * 
         * @param columnData An array of content to be displayed of type T
         */
        export function MakeColumnGroup<T>(cols: number, columnData: T[], addTypeColumn?: boolean) {
            minionGUI.Columns(cols);
            for (let i = 0; i < columnData.length; i++) {
                if (typeof columnData[i] == "object") {
                    API.ObjectForEach((columnData[i] as { [key: string]: any }), (k, v, idx) => {
                        minionGUI.Text(v != undefined && tostring(v) || "undefined");
                        minionGUI.NextColumn();
                        if (addTypeColumn && idx + 1 == cols - 1) {
                            minionGUI.Text(v != undefined && typeof v || "undefined");
                            minionGUI.NextColumn();
                        }
                    })
                } else {
                    minionGUI.Text(tostring(columnData[i]));
                    minionGUI.NextColumn();
                    if (addTypeColumn && (i + 1) % (columnData.length / cols - 1) == 0) {
                        minionGUI.Text(columnData[i] != undefined && typeof columnData[i] || "undefined");
                        minionGUI.NextColumn();
                    }
                }
            }
        }
    }
    export const Misc = {};
    export const ACRs: {}[] = [];
    const dev = {
        active: true,
        acrPath: 'E:\\Git\\QtBard\\AddOn\\'
    }
    export namespace Options {
        export namespace Checkboxes {
            export const AutoPressYes = new Checkbox("AutoPressYes", false, {
                description: "Automatically press the YES on any dialog with YES on it."
            });
            export const ExtractMateria = new Checkbox("Extract Materia", false, {
                description: "Automatically Extract materia from any item on your character"
            });
        }
        export namespace MultiLineInputFields {
            export const AutoPressYesFields = new InputTextMultiLine("PressYesFields", "", 220, 110, {
                description: "Text strings to check before pressing the Yes button on a Yes/No Dialogue. Comma seperated"
            }, minionGUI.InputTextFlags_CtrlEnterForNewLine + minionGUI.InputTextFlags_EnterReturnsTrue);
        }
    }
    export function LogWindow(debugData: {
        id: string,
        value: number | string | boolean | undefined
    }[]) {
        Helpers.MakeColumnGroup(3, debugData, true);
    }
    // Check if the Settings folder exists
    function SettingsFolderExists(): boolean {
        return FolderExists(moduleSettingPath);
    }
    // Check if the Settings file exists
    function SettingsExist(): boolean {
        return FileExists(settingsPath);
    }
    export function IsMainWindowOpen(): boolean {
        return GUI.Open;
    }
    export function ToggleMainWindow(): void {
        GUI.Open = !GUI.Open;
    }
    // Dump to the console
    export function Log(this: void, ...log: any): void {
        d("[QtLib]");
        d(log)
        d('----------------------------')
    }
    // Main initializer set on the main init event 

    // Init setting files and folders.
    export function InitSettings() {
        if (!SettingsFolderExists) {
            Log("Creating Folder", moduleSettingPath)
            FolderCreate(moduleSettingPath);
        }
        if (SettingsExist()) {
            settings = FileLoad(settingsPath);
            Log("Loading Settings", minionSettings);
        } else {
            FileSave(settingsPath, defaultSettings);
            // TODO: Check and update these types when the addon works
            settings = [defaultSettings];
            Log("Default Settings", minionSettings);
        }
    }
    export function Save(force?: boolean): void {
        if (SettingsExist() && (force || Now() > saveLastCheck)) {
            saveLastCheck = Now() + 500;
            FileSave(settingsPath, settings)
        }
    }
    // Main pulse
    export function Update(): void {
        if (Options.Checkboxes.AutoPressYes.Checked) {
            PressYesButton();
        }
        if (Options.Checkboxes.ExtractMateria.Checked) {
            ExtractMateria();
        }
    }
    export function PressYesButton(): void {
        const controls = GetControls();
        API.ObjectForEach(controls, (k, v) => {
            if (v.name == "SelectYesno") {
                API.ObjectForEach(v.GetRawData(), (j, l) => {
                    if (l.type == "string") {
                        for (let i = 0; i < Options.MultiLineInputFields.AutoPressYesFields.strings.length; i++) {
                            const thisString = Options.MultiLineInputFields.AutoPressYesFields.strings[i];
                            if (l.value !== "" && (l.value as string).indexOf(thisString) != -1) {
                                v.Action("CheckAccept");
                                v.Action("Yes");
                            }
                            return;

                        }
                    }
                })
            }
        })
    }
    let spiritBondTime = 0;
    let allInventoryEnums = [
        FFXIV.INVENTORYTYPE.INV_EQUIPPED,
        FFXIV.INVENTORYTYPE.INV_ARMORY_BODY,
        FFXIV.INVENTORYTYPE.INV_ARMORY_EARS,
        FFXIV.INVENTORYTYPE.INV_ARMORY_FEET,
        FFXIV.INVENTORYTYPE.INV_ARMORY_LEGS,
        FFXIV.INVENTORYTYPE.INV_ARMORY_MAINHAND,
        FFXIV.INVENTORYTYPE.INV_ARMORY_HANDS,
        FFXIV.INVENTORYTYPE.INV_ARMORY_HEAD,
        FFXIV.INVENTORYTYPE.INV_ARMORY_NECK,
        FFXIV.INVENTORYTYPE.INV_ARMORY_OFFHAND,
        FFXIV.INVENTORYTYPE.INV_ARMORY_RINGS,
        FFXIV.INVENTORYTYPE.INV_ARMORY_WAIST,
        FFXIV.INVENTORYTYPE.INV_ARMORY_WRIST,
    ]
    export function ExtractMateria(): void {
        if (Now() < spiritBondTime) {
            Log("Returning in Extract Materia because Now is less than spirit bond next check time", Now(), spiritBondTime, Now() < spiritBondTime);
            return;
        }
        for (let i = 0; i < allInventoryEnums.length; i++) {
            const thisInventory = allInventoryEnums[i];
            const allItems = Inventory.Get(thisInventory);
            const itemList = allItems && allItems.GetList();
            if (itemList) {
                // Typescript can be annoying when you cant change the name of API that isnt your own
                for (let j = 0; j < itemList.length; j++) {
                    const item = itemList[j];
                    if (item) {
                        if (item.spiritbond == 100) {
                            item.Convert();
                            spiritBondTime = Now() + 2000;
                            return;
                        }
                    }
                }
            }
        }

    }
    export function MainWindow(event?: any, tickcount?: number): void {
        if (GUI.Open) {
            const flags = minionGUI.WindowFlags_NoScrollbar + minionGUI.WindowFlags_NoResize;
            minionGUI.SetNextWindowSize(style.mainWindow.size.width, style.mainWindow.size.height, minionGUI.SetCond_Always)
            const [visible, open] = minionGUI.Begin(info.addonName, GUI.Open, flags);
            GUI.Visible = visible;
            GUI.Open = open;
            const [tabIndex, tabName] = GUI_DrawTabs(style.mainWindow.components.mainTabs);
            if (tabIndex == 1) {
                Options.Checkboxes.AutoPressYes.Draw();
                Options.MultiLineInputFields.AutoPressYesFields.Draw();
                Options.Checkboxes.ExtractMateria.Draw();

                Options.Checkboxes.AutoPressYes.Save();
                Options.Checkboxes.ExtractMateria.Save();
                Options.MultiLineInputFields.AutoPressYesFields.Save();
            }
            if (tabIndex == 2) {
                // Tab 2 stuff
            }
            if (tabIndex == 3) {
                // Tab 3 stuff
            }
            minionGUI.End();
        }
    }
    export function Init(): void {
        InitSettings();
        Log("QT Rotations Initializing");
        const moduleTable = GUI;
        ml_gui.ui_mgr.AddMember({
            id: info.className,
            name: info.className,
            onClick: () => {
                if (moduleTable.OnClick) {
                    moduleTable.OnClick();
                }
            },
            tooltip: moduleTable.ToolTip,
            texture: "",
        }, "FFXIVMINION##MENU_HEADER")
    }
}


