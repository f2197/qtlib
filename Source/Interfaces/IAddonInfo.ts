export interface IAddonInfo {
    Author: string,
    AddonName: string,
    ClassName: string,
    Version: number,
    StartDate: string,
    LastUpdate: string,
    Description: string,
    Changelog: {
        Version: string, Description: string
    }[]
}