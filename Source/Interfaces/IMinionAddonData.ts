import { IAddonInfo } from "./IAddonInfo";
import { IGUI } from "./UserInterface/IGUI";
import { ISetting } from "./UserInterface/ISetting";
import { IWindowStyle } from "./UserInterface/IWindowStyle";

// Content //

export interface IMinionAddonData {
    Info: IAddonInfo,
    LuaPath: string,
    MinionSettings: string,
    ModulePath: string,
    ModuleSettingPath: string,
    SettingsPath: string,
    DefaultSettings: ISetting,
    Style: {
        MainWindow: IWindowStyle
    },
    Helpers?: {},
    Misc?: {},
    SaveLastCheck: number,
    GUI: IGUI,
}