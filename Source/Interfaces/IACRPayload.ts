import { IACRGUI } from "./IACRGUI";

// Content //


export interface IACRPayload {
    GUI: IACRGUI;
    classes: {
        [key in FFXIV.GAMESTATE]: boolean;
    };

}