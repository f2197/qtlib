export interface IWindowStyle {
    Size: { Width: number, Height: number },
    Components: { MainTabs: any }, // Implement GUI_CreateTabs
}