export interface IGUI {
    Open: boolean;
    Visible: boolean;
    OnClick?: () => void;
    IsOpen?: () => boolean;
    ToolTip: string;
}