import { TOption } from "../../Types/TOption";
import { ITooltip } from "./ITooltip";

// Content //

export interface IOptionPayload {
    identifier: string;
    value: boolean | number | string;
    type: TOption;
    flags?: number;
    tooltip?: ITooltip;
}