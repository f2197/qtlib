import { Entities } from "../API/Entities";
import { Core } from "../Core";
import { Math } from "../API/Math";

// Content //
export class Unit {
    public base: GameObject;
    constructor(baseUnit: GameObject) {
        this.base = baseUnit;
    }
    get Identifier(): number {
        return this.base.id;
    }
    // Bar Data
    get Health(): number {
        return this.base.hp.current;
    }
    get HealthPercentage(): number {
        return this.base.hp.percent;
    }
    get Mana(): number {
        return this.base.mp.current;
    }
    get ManaPercentage(): number {
        return this.base.mp.percent;
    }
    get Gp(): number {
        return this.base.gp.current;
    }
    get GpPercentage(): number {
        return this.base.gp.percent;
    }
    get Cp(): number {
        return this.base.cp.current;
    }
    get CpPercentage(): number {
        return this.base.cp.percent;
    }
    get CastingInfo(): ICastingInfo | undefined {
        return this.base.castinginfo
    }
    public Aura(id: number) {
        return HasBuff(this.base, id);
    }

    public DistanceTo(other: Unit): number {
        return Math.GetDistanceBetweenObjects(this.base, other.base);
    }
    public UnitsAround(dist: number, filter?: string): GameObject[] {
        return Entities.UnitsAroundUnit(this.base, dist, filter);
    }
    public NumUnitsAround(dist: number, filter?: string): number {
        return Entities.NumUnitsAroundUnit(this.base, dist, filter);
    }
    // need to work on this to work better in ff, doesnt really work rn
    public UnitTimeToX(HealthPercentage: number): number {
        let timeToDie = 0;
        if (this.HealthPercentage <= HealthPercentage) {
            timeToDie = 0;
        } else if (this.HealthPercentage == 100 || this.HealthPercentage == 0) {
            timeToDie = 9999;
        } else {
            if (Entities.UnitHealthTracker[this.Identifier]) {
                const HealthDifference = Entities.UnitHealthTracker[this.Identifier].Health - this.HealthPercentage;
                const TimeDifference = Now() - Entities.UnitHealthTracker[this.Identifier].Time;
                const HealthPerTime = HealthDifference / TimeDifference;
                const TimeToDie = (this.HealthPercentage - HealthPercentage) / HealthPerTime;
                // Core.Debug(HealthDifference, "-", TimeDifference, "-", HealthPerTime, "-", TimeToDie, "-", );
                timeToDie = math.ceil(math.min(math.max(TimeToDie, 0), 9999) * 10) / 10;
                // Core.Log("UnitId", this.Identifier, "HPP: ", this.HealthPercentage, "HPDiff", HealthDifference, "TimeDiff", TimeDifference, "HealthPerTime", HealthPerTime, "TTD: ", TimeToDie, "Final TTD", timeToDie);

            } else if (!Entities.UnitHealthTracker[this.Identifier]) {
                Entities.UnitHealthTracker[this.Identifier] = {
                    Time: Now(),
                    Health: this.HealthPercentage
                };
                timeToDie = 9999;
            } else {
                timeToDie = 9999;
            }
        }
        return timeToDie;
    }
}