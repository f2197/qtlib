import { IMainWindowPayload, MainWindow } from "./MainWindow";


// Content //

interface ITabbedWindow extends IMainWindowPayload {
    tabs: ITabs;
}
export class TabbedWindow extends MainWindow {
    public components: { tabs: ITabs; };
    constructor(payload: ITabbedWindow) {
        super({
            size: payload.size
        });
        this.components = {
            tabs: payload.tabs
        };
    }
    public DrawTabs() {
        return GUI_DrawTabs(this.components.tabs);
    }
}