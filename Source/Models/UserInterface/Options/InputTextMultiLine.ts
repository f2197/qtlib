import { ITooltip } from "../../../Interfaces/UserInterface/ITooltip";
import { Option } from "./Abstracts/Option";

// Content //
export class InputTextMultiLine extends Option {
    public strings: string[] = [];
    public sizeX?: number;
    public sizeY?: number;
    constructor(identifier: string, defaultValue: string, sizeX?: number, sizeY?: number, tooltip?: ITooltip, flags?: number) {
        super({
            identifier: identifier,
            type: "InputTextMultiLine",
            value: defaultValue,
            flags: flags,
            tooltip: tooltip
        })
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }
    public Draw(): void {
        const [val, changed] = GUI.InputTextMultiline(this.identifier, (this.Value as string), this.sizeX, this.sizeY, this.flags);
        this.value = val;
        if (changed) {
            this.Value = val;
        }
        if (this.tooltip) {
            if (GUI.IsItemHovered()) {
                GUI.BeginTooltip();
                GUI.SetTooltip(this.tooltip.description);
                GUI.EndTooltip();
            }
        }
    }
    get Value(): number | boolean | string {
        return this.value;
    }
    set Value(val: number | string | boolean) {
        this.value = val;
        this.updateAndSanitizeStrings();
        d(this.type + " Set Value: " + this.settingName + "to: " + val);
        Settings.FFXIVMINION[this.settingName] = val;
    }
    public Save(): void {
        if (ffxivminion.GetSetting(this.settingName) != this.Value) {
            this.updateAndSanitizeStrings();
            d("Save Setting: " + this.settingName + "to: " + this.Value);
            Settings.FFXIVMINION[this.settingName] = this.Value;
        }
    }
    public updateAndSanitizeStrings(): void {
        this.strings = (this.Value as string).split(",");
        for (let i = 0; i < this.strings.length; i++) {
            this.strings[i] = this.strings[i].trim();
        }
        d(this.strings);
    }
}