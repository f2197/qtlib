import { IOptionPayload } from "../../../Interfaces/UserInterface/IOptionPayload";
import { TOption } from "../../../Types/TOption";
import { Option } from "./Abstracts/Option";

// Content //


interface IValuePayload {
    identifier: string;
    value: number;
    type: TOption;
}
export class InputInt extends Option {
    private defaultValue: number = 0;
    constructor(identifier: string, value: number) {
        super({
            identifier: identifier,
            type: "InputInt",
            value: value
        });
    }
    Draw(): void {
        GUI.Text(this.identifier);
        GUI.SameLine(GUI.GetContentRegionAvailWidth() - 100);
        this.value = GUI.InputInt(this.identifier + "Input", this.Value as number, 1)[0];
        if (this.tooltip) {
            if (GUI.IsItemHovered()) {
                GUI.BeginTooltip();
                GUI.SetTooltip(this.tooltip.description);
                GUI.EndTooltip();
            }
        }
    }
}