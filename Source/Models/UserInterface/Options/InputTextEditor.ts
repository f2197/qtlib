import { ITooltip } from "../../../Interfaces/UserInterface/ITooltip";
import { Option } from "./Abstracts/Option";

// Content //
export class InputTextEditor extends Option {
    constructor(identifier: string, defaultValue: string, tooltip?: ITooltip) {
        super({
            identifier: identifier,
            type: "InputText",
            value: defaultValue,
            tooltip: tooltip
        })
    }
    public Draw(): void {
        this.value = GUI.InputTextEditor(this.identifier, (this.Value as string));
        if (this.tooltip) {
            if (GUI.IsItemHovered()) {
                GUI.BeginTooltip();
                GUI.SetTooltip(this.tooltip.description);
                GUI.EndTooltip();
            }
        }
    }
}