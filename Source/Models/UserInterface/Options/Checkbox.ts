import { ITooltip } from "../../../Interfaces/UserInterface/ITooltip";
import { Option } from "./Abstracts/Option";

// Content //

export class Checkbox extends Option {
    constructor(identifier: string, value: boolean, tooltip?: ITooltip) {
        super({
            identifier: identifier,
            value: value,
            type: "Checkbox",
            tooltip: tooltip
        });
    }
    public Draw(): void {
        this.value = GUI.Checkbox(this.identifier, this.Value)[0];
        if (this.tooltip) {
            if (GUI.IsItemHovered()) {
                GUI.BeginTooltip();
                GUI.SetTooltip(this.tooltip.description);
                GUI.EndTooltip();
            }
        }
    }
    get Checked(): boolean {
        return this.Value == true;
    }
}