import { Core } from "../../../../Core";
import { IOptionPayload } from "../../../../Interfaces/UserInterface/IOptionPayload";
import { ITooltip } from "../../../../Interfaces/UserInterface/ITooltip";
import { TOption } from "../../../../Types/TOption";

// Content //
export abstract class Option {
    public identifier: string;
    public value: boolean | string | number;
    public settingName: string;
    public type?: TOption;
    public flags?: number;
    public tooltip?: ITooltip
    constructor(payload: IOptionPayload) {
        // Identifier, Default, Type
        this.identifier = payload.identifier;
        this.settingName = "qtlib" + this.identifier
        this.value = ffxivminion.GetSetting(this.settingName, payload.value);
        this.type = payload.type;
        this.tooltip = payload.tooltip;
        this.flags = payload.flags
    }
    abstract Draw(): void;
    get Value(): number | boolean | string {
        return this.value;
    }
    set Value(val: number | string | boolean) {
        this.value = val;
        d(this.type + " Set Value: " + this.settingName + "to: " + val);
        Settings.FFXIVMINION[this.settingName] = val;
    }
    public Save(): void {
        if (ffxivminion.GetSetting(this.settingName) != this.Value) {
            d("Save Setting: " + this.settingName + "to: " + this.Value);
            Settings.FFXIVMINION[this.settingName] = this.Value;
        }
    }
}