
// Content //


export interface IMainWindowPayload {
    size: {
        width: number,
        height: number
    };
}

export class MainWindow {
    public size: { width: number, height: number };
    constructor(payload: IMainWindowPayload) {
        this.size = payload.size;
    }
}