
import { Option } from "./Options/Abstracts/Option";

// Content //

export class Page {
    public identifier: string;
    public Options: Option[] = [];
    constructor(identifier: string) {
        this.identifier = identifier;
    }
    public DrawOptions(): void {

    }
}