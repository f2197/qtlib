import { Entities } from "../API/Entities";
import { Core } from "../Core";
import { TargetModel } from "./TargetModel";
import { Unit } from "./Unit";

// Content //
export class PlayerModel extends Unit {
    declare base: PlayerUnit;
    constructor(baseUnit: PlayerUnit) {
        super(baseUnit);
    }
    get IsBusy(): boolean {
        return Busy();
    }
    get ComboTimeRemaining(): number {
        return this.base.combotimeremain;
    }
    get Target(): TargetModel | undefined {
        Core.Target.Update();
        if (Core.Target.IsValid) {
            return Core.Target;
        }
        return undefined;
    }
    public GetGauge(idx: number): number {
        return this.base.gauge[idx] != undefined && this.base.gauge[idx] || -1;
    }
}