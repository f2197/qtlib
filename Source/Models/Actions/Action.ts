
import { Entities } from "../../API/Entities";
import { Core } from "../../Core";

// Content //


interface IAdditionalParam {

}
interface IActionPayload {
    identifier: number;
    prevComboAction?: Action,
    hostile?: boolean;
    instant?: boolean;
    offGcd?: boolean;
    auraId?: number;
}

export class Action {
    public identifier: number;
    public type: FFXIV.ACTIONTYPE;
    private auraId?: number;
    public hostile?: boolean;
    public instant?: boolean;
    public offGcd?: boolean;
    public action: PlayerAction;
    public comboAction?: Action;
    public lastCastTime: number = 0;
    constructor(payload: IActionPayload) {
        this.identifier = payload.identifier;
        this.type = 1;
        this.hostile = payload.hostile
        this.instant = payload.instant
        this.offGcd = payload.offGcd
        this.auraId = payload.auraId
        this.comboAction = payload.prevComboAction
        this.action = this.Init();
    }
    get Cooldown(): number {
        return this.action.cdmax - this.action.cd;
    }
    get IsUsable(): boolean {
        return this.action.usable;
    }
    get IsOnCooldown(): boolean {
        return this.action.cd > 0.1
    }
    get TimeSinceCast(): number {
        return (Now() - this.lastCastTime) / 1000;
    }
    get Units(): GameObject[] {
        return Core.Player.UnitsAround(this.action.range);
    }
    get UnitsCount(): number {
        return Core.Player.NumUnitsAround(this.action.range);
    }

    public Init() {
        return ActionList.Get(this.type, this.identifier);
    }
    public CanCombo(): boolean {
        return this.comboAction && Core.Player.ComboTimeRemaining - this.comboAction.TimeSinceCast > 0 && Core.Player.ComboTimeRemaining > 0 || false;
    }
    public CanCast(targetId: number): boolean {
        if (this.IsUsable) {
            if (!this.IsOnCooldown) {
                return true;
            }
        }
        return false;
    }
    get AuraId(): number {
        return this.auraId ? this.auraId : -1;
    }
    public Cast(targetId: number): boolean {
        this.lastCastTime = Now();
        return this.action.Cast(targetId);
    }
}