import { Entities } from "../API/Entities";
import { Unit } from "./Unit";

// Content //

export class TargetModel extends Unit {
    private valid = false
    private lastUpdate = 0;
    constructor(unit: GameObject) {
        super(unit);
        this.Update();
        this.lastUpdate = Now();
    }
    get IsValid(): boolean {
        this.Update();
        return this.valid;
    }
    public Update(): void {
        const newTarget = MGetTarget();
        if (newTarget) {
            this.base = newTarget;
            this.valid = true;
            this.lastUpdate = Now();
            return;
        } else {
            this.valid = false;
        }
    }
}