import { API } from "./API";
import { Core } from "../Core";
import { Math } from "./Math";
import { PlayerModel } from "../Models/PlayerModel";
import { Unit } from "../Models/Unit";
import { TargetModel } from "../Models/TargetModel";

// Content //


export namespace Entities {
    export function UnitsAroundUnit(this: void, unit: GameObject, dist: number, filter?: string): GameObject[] {
        const units: GameObject[] = [];
        const allUnits = EntityList(filter && filter || "alive,los,targetable,attackable,maxdistance=40");
        if (allUnits) {
            API.ObjectForEach(allUnits, (id, obj) => {
                if (unit.id != obj.id && Math.GetDistanceBetweenObjects(unit, obj) < dist) {
                    units.push(obj);
                }
            })
        }
        return units;
    }
    export function NumUnitsAroundUnit(this: void, unit: GameObject, dist: number, filter?: string): number {
        let units = 0;
        const allUnits = EntityList(filter && filter || "alive,los,targetable,attackable,maxdistance=40");
        if (allUnits) {
            API.ObjectForEach(allUnits, (id, obj) => {
                if (unit.id != obj.id && Math.GetDistanceBetweenObjects(unit, obj) < dist) {
                    units++;
                }
            })
        }
        return units;
    }
    export const UnitHealthTracker: {
        [id: string]: {
            Time: number,
            Health: number
        }
    } = {};

}