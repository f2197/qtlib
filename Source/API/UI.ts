// Content //

export namespace UI {
    export function AddColumnText(this: void, text: string): void {
        GUI.Text(text); GUI.NextColumn();
    }
}