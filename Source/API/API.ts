


// Content //


export namespace API {
    export function ObjectForEach<T>(obj: { [key: string]: T }, callback: (k: string | number, v: T, idx: number) => any) {
        if (obj) {
            const keys = Object.keys(obj);
            for (let i = 0; i < keys.length; i++) {
                callback(keys[i], obj[keys[i]] as unknown as T, i);
            }
        }
    }
    export function ArrayForEach<T>(obj: { [key: string]: T }[], callback: (k: string | number, v: T) => any) {
        if (obj) {
            for (let i = 0; i < obj.length; i++) {
                return ObjectForEach(obj[i], callback);
            }
        }
    }

}