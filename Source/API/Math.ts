import { Core } from "../Core";
import { Unit } from "../Models/Unit";


// Content //

export namespace Math {
    export function GetDistanceBetweenPositions(this: void, Position1: I3DCoordinate, Position2: I3DCoordinate): number {
        if (Position2 && Position2.x && Position1 && Position1.x) {
            return math.sqrt(math.pow(Position2.x - Position1.x, 2) + math.pow(Position2.y - Position1.y, 2) + math.pow(Position2.z - Position1.z, 2));
        } else {
            return 100;
        }
    }
    export function GetDistanceBetweenObjects(this: void, object1: GameObject, object2: GameObject): number {
        // If we got a valid save, use it
        if (Core.DistancesTable[object1.id] != undefined && Core.DistancesTable[object1.id][object2.id] != undefined && Core.DistancesTable[object1.id][object2.id][0] > Now()) {
            // d("CreatedCount", CreatedCount, "RecycledCount", RecycledCount);
            return Core.DistancesTable[object1.id][object2.id][1];
        } else if (Core.DistancesTable[object2.id] != undefined && Core.DistancesTable[object2.id][object1.id] != undefined && Core.DistancesTable[object2.id][object1.id][0] > Now()) {
            // d("CreatedCount", CreatedCount, "RecycledCount", RecycledCount);
            return Core.DistancesTable[object2.id][object1.id][1];
        } else {
            const Object1Position = object1.pos;
            const Object2Position = object2.pos;
            if (Core.DistancesTable[object1.id] == undefined) {
                Core.DistancesTable[object1.id] = { [object2.id]: [Now() + 0.2, GetDistanceBetweenPositions(Object1Position, Object2Position)] };
            } else if (Core.DistancesTable[object1.id][object2.id] == undefined) {
                Core.DistancesTable[object1.id][object2.id] = [Now() + 0.2, GetDistanceBetweenPositions(Object1Position, Object2Position)];
            } else {
                Core.DistancesTable[object1.id][object2.id][0] = Now() + 0.2;
                Core.DistancesTable[object1.id][object2.id][1] = GetDistanceBetweenPositions(Object1Position, Object2Position);
            }
            // d("CreatedCount", CreatedCount, "RecycledCount", RecycledCount);
            return Core.DistancesTable[object1.id][object2.id][1];
        }
    }
}